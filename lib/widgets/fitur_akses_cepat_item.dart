import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class FiturAksesCepatItem extends StatelessWidget {
  const FiturAksesCepatItem({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 345,
      height: 110,
      decoration: BoxDecoration(
        border: Border.all(color: const Color(0xffE8E8E8)),
        borderRadius: BorderRadius.circular(15),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 10, right: 10, top: 5),
            child: Row(
              children: [
                Padding(
                  padding: const EdgeInsets.only(right: 10.0),
                  child: SizedBox(
                      height: 24,
                      width: 24,
                      child: SvgPicture.asset('assets/svgs/goride-icon.svg')),
                ),
                const Text(
                  'Pintu masuk motor, MNC Land',
                  style: TextStyle(fontSize: 14, color: Color(0xff4A4A4A)),
                ),
                const Spacer(),
                SvgPicture.asset('assets/svgs/arrowright-icon.svg')
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 10, right: 10, top: 5),
            child: Row(
              children: [
                Padding(
                  padding: const EdgeInsets.only(right: 10.0),
                  child: SizedBox(
                      height: 24,
                      width: 24,
                      child: SvgPicture.asset('assets/svgs/goride-icon.svg')),
                ),
                const Text(
                  'Pintu masuk motor, MNC Land',
                  style: TextStyle(fontSize: 14, color: Color(0xff4A4A4A)),
                ),
                const Spacer(),
                SvgPicture.asset('assets/svgs/arrowright-icon.svg')
              ],
            ),
          )
        ],
      ),
    );
  }
}
