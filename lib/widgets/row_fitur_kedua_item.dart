import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class RowFiturKeduaItem extends StatelessWidget {
  const RowFiturKeduaItem({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 320,
      height: 64,
      child: Row(
        children: [
          //membuat fitur gomart pada aplikasi
          Column(
            children: [
              SvgPicture.asset('assets/svgs/gomart-icon.svg'),
              const Spacer(),
              const Text(
                'GoMart',
                style: TextStyle(fontSize: 12.5, color: Color(0xff4A4A4A)),
              )
            ],
          ),
          const Spacer(),
          //membuat fitur gopulsa pada aplikasi
          Column(
            children: [
              SvgPicture.asset('assets/svgs/gopulsa-icon.svg'),
              const Spacer(),
              const Text(
                'GoPulsa',
                style: TextStyle(fontSize: 12.5, color: Color(0xff4A4A4A)),
              )
            ],
          ),

          const Spacer(),
          //membuat fitur goclub pada aplikasi
          Column(
            children: [
              SvgPicture.asset('assets/svgs/goclub-icon.svg'),
              const Spacer(),
              const Text(
                'GoClub',
                style: TextStyle(fontSize: 12.5, color: Color(0xff4A4A4A)),
              )
            ],
          ),
          const Spacer(),
          //membuat fitur lainnya pada aplikasi
          Column(
            children: [
              SvgPicture.asset('assets/svgs/lainnya-icon.svg'),
              const Spacer(),
              const Text(
                'Lainnya',
                style: TextStyle(fontSize: 12.5, color: Color(0xff4A4A4A)),
              )
            ],
          ),
        ],
      ),
    );
  }
}
