import 'package:flutter/material.dart';

class FiturGopayLaterItem extends StatelessWidget {
  const FiturGopayLaterItem({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      //membuat fitur gopaylater
      height: 83,
      width: 324,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
              //foto gopaylater
              height: 14,
              width: 96,
              child: Image.asset(
                'assets/images/gopaylater.png',
                fit: BoxFit.cover,
              )),
          const SizedBox(
            height: 8,
          ),
          //membuat text dan juga emoticon
          Row(
            children: [
              const Text(
                'Lebih hemat pake GoPayLater',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 16,
                    color: Color(0xff1D1D1D)),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 3.0),
                child: SizedBox(
                  width: 17,
                  height: 16,
                  child: Image.asset(
                    'assets/images/emoticon.png',
                    fit: BoxFit.cover,
                  ),
                ),
              )
            ],
          ),
          const SizedBox(
            height: 8,
          ),
          //membuat text terakhir
          const Text(
            'Yuk, belanja di Tokopedia pake GoPay Later dan nikmatin cashback-nya~',
            style: TextStyle(fontSize: 14, color: Color(0xff4A4A4A)),
          )
        ],
      ),
    );
  }
}
