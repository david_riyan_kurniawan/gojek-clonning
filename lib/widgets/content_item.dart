import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ContentItem extends StatefulWidget {
  const ContentItem({
    super.key,
  });

  @override
  State<ContentItem> createState() => _ContentItemState();
}

class _ContentItemState extends State<ContentItem> {
  //membuat function sebagai index di corousel sliders
  int _current = 0;
  final CarouselController _controller = CarouselController();
  List<Widget> app = [
    Container(
      width: 127,
      height: 68,
      decoration: BoxDecoration(
        color: const Color(0xff9CCDDB),
        borderRadius: BorderRadius.circular(8),
      ),
    ),
    Container(
      width: 127,
      height: 68,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(8),
      ),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Image.asset('assets/images/gopay-icon.png'),
            const Text(
              'Rp12.379',
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                  color: Color(0xff1D1D1D)),
            ),
            const Text(
              'Klik & cek riwayat',
              style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 12.5,
                  color: Color(0xff0C9B16)),
            ),
          ],
        ),
      ),
    )
  ];
  @override
  Widget build(BuildContext context) {
    return Container(
      //membuat isi content yang terdiri dari card event, fitur bayar, fitur top up, dan fitur eksplor
      width: 345,
      height: 96,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          color: const Color(0xff0281A0)),
      child: Row(
        children: [
          //membuat indicator pada sliders
          Padding(
            padding: const EdgeInsets.only(left: 8.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: app.asMap().entries.map((entry) {
                return GestureDetector(
                  onTap: () => _controller.animateToPage(entry.key),
                  child: Container(
                    width: 2.0,
                    height: 8.0,
                    margin: const EdgeInsets.symmetric(
                      vertical: 2.0,
                    ),
                    decoration: BoxDecoration(
                        color: (Theme.of(context).brightness == Brightness.dark
                                ? Colors.white
                                : Colors.white)
                            .withOpacity(_current == entry.key ? 0.9 : 0.1)),
                  ),
                );
              }).toList(),
            ),
          ),
          //membuat slidernya
          Padding(
            padding: const EdgeInsets.only(left: 8.0),
            child: CarouselSlider(
              items: app,
              carouselController: _controller,
              options: CarouselOptions(
                  scrollDirection: Axis.vertical,
                  autoPlayAnimationDuration: const Duration(seconds: 2),
                  autoPlay: true,
                  enlargeCenterPage: false,
                  aspectRatio: 1.2,
                  onPageChanged: (index, reason) {
                    setState(() {
                      _current = index;
                    });
                  }),
            ),
          ),

          //membuat icon bayar, top up, eksplor
          Padding(
            padding: const EdgeInsets.only(left: 20.0),
            child: Material(
              borderRadius: BorderRadius.circular(20),
              color: Colors.transparent,
              child: InkWell(
                borderRadius: BorderRadius.circular(20),
                onTap: () {},
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                  ),
                  width: 36,
                  height: 48,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SvgPicture.asset('assets/svgs/bayar-icon.svg'),
                      const SizedBox(
                        height: 5,
                      ),
                      const Text(
                        'Bayar',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 14,
                            fontWeight: FontWeight.w500),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          //membuat icon top up
          Padding(
            padding: const EdgeInsets.only(left: 20.0),
            child: Material(
              borderRadius: BorderRadius.circular(20),
              color: Colors.transparent,
              child: InkWell(
                borderRadius: BorderRadius.circular(20),
                onTap: () {},
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                  ),
                  width: 44,
                  height: 48,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SvgPicture.asset('assets/svgs/topup-icon.svg'),
                      const SizedBox(
                        height: 5,
                      ),
                      const Text(
                        'TopUp',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 14,
                            fontWeight: FontWeight.w500),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          //icon eksplor
          Padding(
            padding: const EdgeInsets.only(left: 20.0),
            child: Material(
              borderRadius: BorderRadius.circular(20),
              color: Colors.transparent,
              child: InkWell(
                borderRadius: BorderRadius.circular(20),
                onTap: () {},
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                  ),
                  width: 47,
                  height: 48,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SvgPicture.asset('assets/svgs/eksplor-icon.svg'),
                      const SizedBox(
                        height: 5,
                      ),
                      const Text(
                        'Eksplor',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 14,
                            fontWeight: FontWeight.w500),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
