import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class EventItem extends StatelessWidget {
  const EventItem({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          margin: const EdgeInsets.symmetric(horizontal: 20),
          height: 65,
          width: 345,
          decoration: BoxDecoration(
              border: Border.all(color: const Color(0xffE8E8E8)),
              borderRadius: BorderRadius.circular(15)),
        ),
        Padding(
          padding: const EdgeInsets.all(4.0),
          child: Container(
            margin: const EdgeInsets.symmetric(horizontal: 20),
            width: 120,
            height: 57,
            decoration: BoxDecoration(
                image: const DecorationImage(
                    image: AssetImage('assets/images/dots.png')),
                borderRadius: BorderRadius.circular(15),
                gradient: const LinearGradient(
                    colors: [Color(0xffBBE7F0), Colors.white],
                    begin: Alignment.centerLeft,
                    end: Alignment.centerRight)),
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              SizedBox(
                height: 40,
                width: 40,
                child: Image.asset('assets/images/icon-star.png'),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text(
                    '117 XP lagi ada Harta Karun',
                    style: TextStyle(fontSize: 14, fontWeight: FontWeight.w600),
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  Stack(
                    children: [
                      Container(
                        width: 209,
                        height: 4,
                        color: Colors.grey,
                      ),
                      Container(
                        width: 120,
                        height: 4,
                        color: Colors.green,
                      ),
                    ],
                  )
                ],
              ),
              SvgPicture.asset('assets/svgs/arrowright-icon.svg')
            ],
          ),
        )
      ],
    );
  }
}
