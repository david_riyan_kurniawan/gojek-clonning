import 'package:flutter/material.dart';

class Contentiklan2Item extends StatelessWidget {
  const Contentiklan2Item({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      //membuat container untuk memberikan efek border radius dan juga warna border
      width: 343,
      height: 285,
      decoration: BoxDecoration(
          border: Border.all(color: const Color(0xffE8E8E8)),
          borderRadius: BorderRadius.circular(15)),
      child: Column(
        children: [
          //menambahkan image pada container
          Container(
            height: 170,
            width: 343,
            decoration: const BoxDecoration(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(15),
                    topRight: Radius.circular(15)),
                image: DecorationImage(
                    image: AssetImage('assets/images/gambar2.png'))),
          ),
          const SizedBox(
            height: 20,
          ),
          //menambahkan text dan emoticon
          Padding(
            padding: const EdgeInsets.only(left: 20.0),
            child: Row(
              children: [
                const Text(
                  'Makin Seru',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 3.0),
                  child: SizedBox(
                    height: 16,
                    width: 16,
                    child: Image.asset('assets/images/emoticon2.png'),
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 8,
          ),
          const Text(
            'Sambungin Akun ke Tokopedia, Banyakin Untung',
            style: TextStyle(fontSize: 14, color: Color(0xff4A4A4A)),
          )
        ],
      ),
    );
  }
}
