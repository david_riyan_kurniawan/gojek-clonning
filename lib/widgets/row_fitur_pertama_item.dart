import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class RowFiturPertamaItem extends StatelessWidget {
  const RowFiturPertamaItem({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 320,
      height: 64,
      //membuat fitur aplikasi pada row kedua
      child: Row(
        children: [
          //membuat fitur goride pada aplikasi
          Column(
            children: [
              SvgPicture.asset('assets/svgs/goride-icon.svg'),
              const Spacer(),
              const Text(
                'GoRide',
                style: TextStyle(fontSize: 12.5, color: Color(0xff4A4A4A)),
              )
            ],
          ),
          const Spacer(),
          //membuat fitur gocar pada aplikasi
          Column(
            children: [
              SvgPicture.asset('assets/svgs/gocar-icon.svg'),
              const Spacer(),
              const Text(
                'GoCar',
                style: TextStyle(fontSize: 12.5, color: Color(0xff4A4A4A)),
              )
            ],
          ),

          const Spacer(),
          //membuat fitur gofood pada aplikasi
          Column(
            children: [
              SvgPicture.asset('assets/svgs/gofood-icon.svg'),
              const Spacer(),
              const Text(
                'GoFood',
                style: TextStyle(fontSize: 12.5, color: Color(0xff4A4A4A)),
              )
            ],
          ),
          const Spacer(),
          //membuat fitur gosend pada aplikasi
          Column(
            children: [
              SvgPicture.asset('assets/svgs/gosen-icon.svg'),
              const Spacer(),
              const Text(
                'GoSend',
                style: TextStyle(fontSize: 12.5, color: Color(0xff4A4A4A)),
              )
            ],
          ),
        ],
      ),
    );
  }
}
