import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class TexfieldavatarItem extends StatelessWidget {
  const TexfieldavatarItem({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 22),
      child: Row(
        children: [
          SizedBox(
            height: 50,
            width: 295,
            child: TextField(
              autocorrect: false,
              decoration: InputDecoration(
                enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(30),
                    borderSide: const BorderSide(color: Color(0xff999798))),
                focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(30),
                    borderSide: const BorderSide(color: Color(0xff999798))),
                prefixIcon: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: SizedBox(
                    height: 20,
                    child: SvgPicture.asset(
                      'assets/svgs/search-icon.svg',
                    ),
                  ),
                ),
                fillColor: Colors.white,
                filled: true,
                hintText: 'Cari layanan, makanan, & tujuan',
                hintStyle:
                    const TextStyle(fontSize: 14, color: Color(0xff999798)),
              ),
            ),
          ),
          const Spacer(),
          Stack(
            alignment: Alignment.bottomRight,
            children: [
              Container(
                width: 35,
                height: 35,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(100),
                    image: const DecorationImage(
                        image: AssetImage('assets/images/david.jpg'),
                        fit: BoxFit.cover)),
              ),
              Container(
                width: 16,
                height: 16,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(100),
                ),
                child: SvgPicture.asset('assets/svgs/avatar-icon.svg'),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
