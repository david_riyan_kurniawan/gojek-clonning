import 'package:flutter/material.dart';
import '../widgets/textfield_avatar.dart';
import '../widgets/content_item.dart';
import '../widgets/row_fitur_pertama_item.dart';
import '../widgets/row_fitur_kedua_item.dart';
import '../widgets/event_item.dart';
import '../widgets/fitur_akses_cepat_item.dart';
import '../widgets/fitur_gopaylater_item.dart';
import '../widgets/Content_iklan_item.dart';
import '../widgets/content_iklan2_item.dart';
import '../widgets/content_iklan3_item.dart';

class BerandaPage extends StatefulWidget {
  const BerandaPage({super.key});

  @override
  State<BerandaPage> createState() => _BerandaPageState();
}

class _BerandaPageState extends State<BerandaPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          Column(
            children: [
              const SizedBox(
                height: 20,
              ),

              const TexfieldavatarItem(), // function ini berisi code textfield pada aplikasi
              const SizedBox(
                height: 13,
              ),
              const ContentItem(),
              const SizedBox(
                height: 25,
              ),
              //row fitur yang pertama
              const RowFiturPertamaItem(),
              const SizedBox(
                height: 20,
              ),
              //membuat fitur aplikasi pada row kedua
              const RowFiturKeduaItem(),
              const SizedBox(
                height: 15,
              ),
              const EventItem(),
              const SizedBox(
                height: 10,
              ),
              Align(
                  alignment: Alignment.centerLeft,
                  child: Container(
                      margin: const EdgeInsets.only(left: 25),
                      child: const Text(
                        'Akses cepat',
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 18,
                            color: Color(0xff000000)),
                      ))),
              const SizedBox(
                height: 10,
              ),
              const FiturAksesCepatItem(),
              const SizedBox(
                height: 20,
              ),
              const FiturGopayLaterItem(),
              const SizedBox(
                height: 23,
              ),
              const ContentiklanItem(),
              const SizedBox(
                height: 20,
              ),
              const Contentiklan2Item(),
              const Contentiklan3Item()
            ],
          )
        ],
      ),
    );
  }
}
