import 'package:flutter/material.dart';
import './beranda_page.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 4,
      child: Scaffold(
        //membuat custom appbar
        appBar: AppBar(
          toolbarHeight: 71,
          backgroundColor: Colors.green[600],
          elevation: 0,
          bottom:
              // preferredsize digunakan untuk membungkus widget, disini widgetnya adalah container, widget ini juga biasa digunakan untuk memberikan style tambahan
              PreferredSize(
            preferredSize: const Size.fromHeight(0),
            child:
                // memberikan jarak antara tabbar dengan toolbar diatasnya
                Positioned(
              top: 2,
              child: Container(
                // memberikan border radius pada container yang berguna untuk memberikan efek radius pada tabbar
                margin: const EdgeInsets.only(bottom: 10),
                height: 50,
                width: 345,
                decoration: BoxDecoration(
                    color: Colors.green[800],
                    borderRadius: BorderRadius.circular(30)),
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(5, 5, 1, 5),
                  child:
                      //membuat fitur tabbar pada home page aplikasi
                      TabBar(
                          isScrollable: true, //agar tabbar dapat discroll
                          splashBorderRadius: BorderRadius.circular(100),
                          labelColor: Colors.green[800],
                          labelStyle: const TextStyle(
                              fontWeight: FontWeight.w700, fontSize: 14),
                          unselectedLabelStyle: const TextStyle(
                              fontWeight: FontWeight.w700, fontSize: 14),
                          unselectedLabelColor: Colors.white,
                          //di indikator diberikan box decoration dimana ini berguna untuk memberikan style pada indicatornya
                          indicator: BoxDecoration(
                              borderRadius: BorderRadius.circular(30),
                              color: Colors.white),
                          tabs: const [
                        Tab(
                          text: 'Beranda',
                        ),
                        Tab(
                          text: 'Promo',
                        ),
                        Tab(
                          text: 'Pesanan',
                        ),
                        Tab(
                          text: 'Chat',
                        ),
                      ]),
                ),
              ),
            ),
          ),
        ),
        body:
            //membuat tabbar view yang mana di tampilkan di bawah appbar
            TabBarView(children: [
          const BerandaPage(), // page pertama pada saat aplikasi dibuka
          Container(),
          Container(),
          Container(),
        ]),
      ),
    );
  }
}
